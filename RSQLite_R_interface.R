#---
#  title: 'Annotation Handling with SQLite'
#author: "Matt Hibberd"
#date: '`r format(Sys.Date(), "%B %d, %Y")`'
#output: html_document
#editor_options: 
#  chunk_output_type: console
#---
  
library(DBI)

# Initialize empty sqlite db
mydb <- dbConnect(RSQLite::SQLite(), "metagenomes.sqlite")
dbDisconnect(mydb)
unlink("metagenomes.sqlite")

# Load table into sqlite db
mydb <- dbConnect(RSQLite::SQLite(), "metagenomes.sqlite")
dbWriteTable(mydb, "mtcars", mtcars)
dbWriteTable(mydb, "iris", iris)
dbListTables(mydb)

# Queries examples
dbGetQuery(mydb, 'SELECT * FROM mtcars LIMIT 5')
dbGetQuery(mydb, 'SELECT * FROM iris WHERE "Sepal.Length" < 4.6') # Sepal.Length isn't a valid SQL variable name
dbGetQuery(mydb, 'SELECT * FROM iris WHERE "Sepal.Length" < :x', params = list(x = 4.6)) # Safer from a SQL injection procedure

# Batch queries (cuts down on memory usage)
rs <- dbSendQuery(mydb, 'SELECT * FROM mtcars')
while (!dbHasCompleted(rs)) {
  df <- dbFetch(rs, n = 10)
  print(nrow(df))
}
dbClearResult(rs)

# Multiple parameterized queries
rs <- dbSendQuery(mydb, 'SELECT * FROM iris WHERE "Sepal.Length" < :x')
dbBind(rs, param = list(x = 4.5))
nrow(dbFetch(rs))
dbBind(rs, param = list(x = 4))
nrow(dbFetch(rs))
dbClearResult(rs)

rs <- dbSendQuery(mydb, 'SELECT * FROM iris WHERE "Sepal.Length" = :x')
dbBind(rs, param = list(x = seq(4, 4.4, by = 0.1)))
nrow(dbFetch(rs))
dbClearResult(rs)

# Updated alternatives to dbSendQuery and dbGetQuery
dbExecute(mydb, 'DELETE FROM iris WHERE "Sepal.Length" < 4')
rs <- dbSendStatement(mydb, 'DELETE FROM iris WHERE "Sepal.Length" < :x')
dbBind(rs, param = list(x = 4.5))
dbGetRowsAffected(rs)
dbClearResult(rs)